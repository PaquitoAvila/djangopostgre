from django.db import models

# Create your models here.
class Acta_Nacimiento(models.Model):
    lugar_nacimiento = models.CharField(max_length=40)
    fecha_modificacion = models.DateField
    hora_nacimiento = models.IntegerField
    primer_nombre = models.CharField(max_length=12)
    segundo_nombre = models.CharField(max_length=12)
    primer_apellido = models.CharField(max_length=12)
    segundo_apellido = models.CharField(max_length=12)
    numero_de_identificacion = models.IntegerField
    fecha_nacimiento = models.DateField


