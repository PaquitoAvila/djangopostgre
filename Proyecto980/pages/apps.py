from django.apps import AppConfig


class ArticulosConfig(AppConfig):
    name = 'articulos'
    verbose_name = 'Articulos'
