from django.http import HttpResponse

def hola_mundo(request):
    cadena_numeros = request.GET['array']
    cadena_numeros = cadena_numeros.split(',')
    cadena_numeros = [int(index) for index in cadena_numeros]
    data = {
        'status' : 'ok',
        'responde' : 'array de numeros',
        'clase' : 'Proyectos 980. doc',
        'array_numeros' : cadena_numeros,
        'mensaje' : 'Termino Proceso',
    }
    return HttpResponse(str(cadena_numeros))
