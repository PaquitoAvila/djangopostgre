FROM debian:latest

RUN mkdir /Proyectos980

WORKDIR /Proyectos980


RUN apt-get update \
    && apt-get upgrade -y && apt-get -y install git \
        postgresql-client python3 python3-pip net-tools nano\
    && rm -rf /var/lib/apt/lists/*

RUN pip3 install django
RUN python3 -m django --version
RUN django-admin startproject Proyecto980 . 

EXPOSE 8000
RUM pip3 install psycopg2
CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]
